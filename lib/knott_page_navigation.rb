# Separate the question of what information to show in the navigation from
# that of how to render it in HTML. Here, we are just determining the "what".

# Knott Fdn web site uses a peculiar secondary menu.

class KnottPageNavigation

  # An instance pertains to a page and assuming that page is the current page
  # we're displaying, calculates the primary and secondary menus to show along
  # with the page. This is just the info, nothing about rendering; that belongs
  # somewhere else.

  # Inputs.

  attr_accessor :global_page_tree
    # Must be the tree of published pages with the home page as its root.
  attr_accessor :view

  # Outputs.

  attr_accessor :primary, :secondary # the output menus.

  class Entry
    attr_accessor :owner
    attr_accessor :traces_to_current
    attr_accessor :tree_node
    def inspect
      indicator = traces_to_current ? "*" : ""
      "{#{indicator}#{page.id} #{tree_node.content.title.inspect}" +
      ", #{children.count}c}"
    end
    def page
      @page ||= tree_node.content
    end
    def entry_for_tree_node a_node
      owner.entry_for_tree_node a_node
    end
    def parent
      @parent ||= entry_for_tree_node tree_node.parent
    end
    def is_root
      tree_node.is_root?
    end
    def title
      page.title
    end
    def url
      page.url
    end
    def children
      @children ||= trustees_suppression ? [] : normal_children
    end

    private

    def normal_children
      tree_node.children.
        select{|s| s.content.published && s.content.show_in_nav}.
        map{|e| entry_for_tree_node e}.
        sort{|a, b| a.page.position <=> b.page.position}
    end
    def trustees_suppression
      page.ptype == 'Trustee' && ! current_trustee && ! current_admin
    end
    def current_trustee; owner.current_trustee; end
    def current_admin;   owner.current_admin;   end
  end

  def entries_by_id
    @entries_by_id ||= {}
  end

  def entry_for_tree_node a_tree_node
    hit = entries_by_id[a_tree_node.content.id]
    unless hit
      hit = Entry.new
      hit.traces_to_current = false # but this may get revised.
      hit.owner = self
      hit.tree_node = a_tree_node
      # entries_by_id[a_tree_node.content.id] = hit
      @entries_by_id = entries_by_id.merge({a_tree_node.content.id => hit})
    end
    hit
  end

  def entry_for_page a_page
    entry_for_tree_node global_page_tree.find a_page
  end

  def root
    @root = entry_for_tree_node global_page_tree.root
  end

  def initialize specs
    self.global_page_tree = specs[:global_page_tree] or
      throw "Required!"
      # Must be the tree of published pages with the home page as its root.
    cur = specs[:current_page]
    self.view = specs[:view]
    self.primary = root.children
    if cur.nil?
      self.secondary = []
    elsif global_page_tree.root.content.id == cur.id # is the home page.
      self.secondary = []
    else
      trace = entry_for_page cur
      while true
        trace.traces_to_current = true
        break if trace.is_root
        trace = trace.parent
      end
      hit = primary.detect &:traces_to_current
      if hit
        self.secondary = hit.children
      else
        # An example where this case happens is the search-results page.
        self.secondary = []
      end
    end
  end

  def current_trustee; view && view.current_trustee; end
  def current_admin;   view && view.current_admin;   end

  def inspect
    "{some page navigation}"
  end
end


jQuery(function() {

  var s = $(".blog #categories");
  var pos = s.position();

  if($(".blog #categories").length) {
    $(window).scroll(function() {
        var windowpos = $(window).scrollTop();
        //s.html("Distance from top:" + pos.top + "<br />Scroll position: " + windowpos);
        if (windowpos >= pos.top) {
            s.addClass("stick");
        } else {
            s.removeClass("stick");
        }
    });
  }
});

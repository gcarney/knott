jQuery(function() {

  jQuery("#image_carousel").slidesjs({
    width: 1175,
    height: 442,

    navigation: {
      active: false
    },

    effect: {
      // slide: {
      //   speed: 2000
      //     // [number] Speed in milliseconds of the slide animation.
      // }
      fade: {
        speed: 300,
          // [number] Speed in milliseconds of the fade animation.
        crossfade: true
          // [boolean] Cross-fade the transition.
      }
    },

    pagination: {
      active: false,
      // effect: 'fade'
    },

    play: {
      active: false,
      auto: true,
      interval: 6000,
      effect: 'fade',
      pauseOnHover: true,
      restartDelay: 2500
    },

    callback: {
      loaded: function(number) {
        console.log( 'Loaded:' + ( number - 1 ) );
        var path = $('#image_carousel').find("a[slidesjs-index='" + ( number - 1 ) + "']").attr('href');
        $('#learn-more').attr('href', path);
      },
      complete: function(number) {
        console.log( 'Running: ' + ( number - 1 ) );
        var path = $('#image_carousel').find("a[slidesjs-index='" + ( number - 1 ) + "']").attr('href');
        $('#learn-more').attr('href', path);
      }
    }
  });

  jQuery('#carousel_wrapper #controls li').click(function() {
    var dot_select = jQuery(this).attr('id').split("_")[0];

    jQuery('#image_carousel li.current').toggleClass('current').hide();
    jQuery('#image_carousel li#' + dot_select).toggleClass('current').show();
  });

  jQuery('ul.slidesjs-pagination a').text('');

});
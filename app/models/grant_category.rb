class GrantCategory < ActiveRecord::Base
  validates :name, presence: true

  has_many :grant_categorizations, :dependent => :destroy
  has_many :grants, :through => :grant_categorizations

  before_save :create_slug

  class << self
    def protected_ids
      {:discretionary => 9}
    end

    def all_category_slugs
      all.select(:slug).group_by(:slug).map &:slug
    end

    def displayable
      all.reject {|gc| GrantCategory.protected_ids.has_value?(gc.id) }
    end
  end

  def create_slug
    self.slug = name.downcase.gsub(/[ \/]/, "-").gsub(/[^a-z \-]/, "")
  end

  def percentage_of_last_5_years
    g_total = Grant.total_for_last_5_years
    @percentage_of_latest_displayable_year ||= g_total == 0 ? 0 : (total_for_last_5_years.to_f / g_total.to_f) * 100
  end

  def total_for_last_5_years
    grants.select{|g| g.year >= Time.now.year-4 && g.year <= Time.now.year}.inject(0){ |sum, g| sum + g.value }
  end

  def total_for_latest_displayable_year
    grants.select{ |g| g.year == GrantYearManager.latest_displayable.year }.inject(0){ |sum, g| sum + g.value }
  end
end

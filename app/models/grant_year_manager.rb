class GrantYearManager < ActiveRecord::Base
  validates :year, presence: true
  validates :show_this_year, inclusion: { :in => [true, false] }

  class << self
    def displayable_years
      where(:show_this_year => true).order('year')
    end

    def latest_displayable
      displayable_years.last
    end
  end
end

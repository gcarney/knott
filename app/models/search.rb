class Search < ActiveRecord::Base
  def self.results(search_term, search_protected_pages = false)
    @results = {}

    pages = PennStation::Page.arel_table
    sections = PennStation::Section.arel_table

    @results[:pages] = PennStation::Page.joins(:sections)
      .where(pages[:published].eq(true))
      .where(sections[:content].matches("%#{search_term}%"))
      .where(pages[:url].does_not_match("/trustees%"))
      .uniq.to_a

    posts = PennStationBlog::Post.arel_table

    @results[:blog_posts] = PennStationBlog::Post
      .where(posts[:publish].eq(true))
      .where(["title LIKE ? OR summary LIKE ? OR body LIKE ?",
             "%#{search_term}%", "%#{search_term}%", "%#{search_term}%"])
      .to_a

    @results[:grants] = Grant.where(["description LIKE ? OR recipient LIKE ?", "%#{search_term}%", "%#{search_term}%"]).to_a

    if search_protected_pages
      @results[:pages] += PennStation::Page.joins(:sections)
        .where(pages[:published].eq(true))
        .where(sections[:content].matches("%#{search_term}%"))
        .where(pages[:url].matches("/trustees%"))
        .uniq.to_a
    end

    @results
  end
end

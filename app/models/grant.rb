class Grant < ActiveRecord::Base
  validates :recipient, presence: true
  validates :year, presence: true
  validates :feature_on_home_page, inclusion: { :in => [true, false] }

  has_many :grant_categorizations
  has_many :grant_categories, :through => :grant_categorizations

  class << self
    def for_years(years)
      where(:year => years).order("year DESC, recipient")
    end

    def with_categories(categories)
      includes(:grant_categories).where("grant_categories.name" => categories).order("grant_categories.name")
    end

    def total_for_last_5_years
      this_year = GrantYearManager.latest_displayable.year
      Grant.where('year <= ? and year >= ?',this_year,this_year-4).sum('value')
    end

    def total_for_latest_displayable_year
      Grant.where(:year => GrantYearManager.latest_displayable.year).sum('value')
    end
  end

  def category_names
    grant_categories.map &:name
  end

end

__END__

class Grant < ActiveRecord::Base

  class << self
    def total_for_latest_displayable_year
      Grant.sum(:value, :conditions => ["year = ?", GrantYearManager.latest_displayable_year])
    end
  end

  belongs_to :uploaded_image
  has_many :grant_categorizations, :include => :grant_category, :dependent => :destroy
  has_many :grant_categories, :through => :grant_categorizations

  has_finder :for_years, lambda { |years| {
    :order      => "year DESC, recipient",
    :conditions => { :year => [years].flatten } } }
  has_finder :with_categories, lambda { |categories| {
    :include    => { :grant_categorizations => :grant_category },
    :order      => "grant_category.name",
    :conditions => { "grant_categories.slug" => [categories].flatten } } }
  has_finder :featured, :order => 'year DESC, recipient', :conditions => ["feature_on_home_page = ?", true]

  attr_accessor :grant_category_ids
  after_save :update_grant_category_ids

  after_save :create_new_grant_year_manager

  validates_presence_of :recipient, :year
  validates_presence_of :uploaded_image, :if => :feature_on_home_page

  def category_names
    grant_categories.map &:name
  end

  def value=(new_value)
    write_attribute :value, new_value.gsub(/[^0-9]/,'')
  end

  def feature_on_home_page
    read_attribute :feature_on_home_page || false
  end

  private
    def update_grant_category_ids
      self.grant_categorizations.clear
      if grant_category_ids.nil?
        self.grant_categorizations.create(:grant_category_id => GrantCategory.protected_ids[:discretionary])
      else
        grant_category_ids.each { |gc_id| self.grant_categorizations.create(:grant_category_id => gc_id) unless gc_id.blank? }
      end
      reload
      self.grant_category_ids = nil
    end

    def create_new_grant_year_manager
      year = self.year
      GrantYearManager.create(:year => year, :show_this_year => false) unless GrantYearManager.exists?(:year => year)
    end
end

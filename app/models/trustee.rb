class Trustee < ActiveRecord::Base
  has_secure_password

  validates :email, presence: true,
                    uniqueness: true

  validates :name, :presence => true,
                   :uniqueness => true

  validates :password, :length => { :minimum => 8, :if => :validate_password? },
                       :confirmation => { :if => :validate_password? }

  validates :password_confirmation, :length => { :minimum => 8, :if => :validate_password? },
                                    :confirmation => { :if => :validate_password? }

  before_create { generate_token(:auth_token) }

  def self.url
    trustee_page = PennStation::Page.where(:ptype => 'Trustee').first
    trustee_page ? trustee_page.url : '/trustees'
  end

  def self.page
    PennStation::Page.where(:ptype => 'Trustee').first
  end

  def activity_info
    "trustee #{name}"
  end

  def send_password_reset
    generate_token(:password_reset_token)
    self.password_reset_sent_at = Time.zone.now
    save!
    TrusteeMailer.password_reset(self).deliver
  end

  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while Trustee.exists?(column => self[column])
  end

  private

  def validate_password?
    password.present? || password_confirmation.present?
  end
end

module NavigationHelper

  def link_for_nav_entry(nav_entry, style_class = nil)
    if style_class.present?
      link_to(nav_entry.title, nav_entry.url, class: style_class)
    else
      link_to(nav_entry.title, nav_entry.url)
    end
  end

  def mobile_nav
    content_tag(:ol, class: 'topnav') do      
      navs = content_tag(:li, (link_to 'Menu', 'javascript:void(0);', onclick: "slideNavigation();")) +
      @nav.primary.inject(h '') do | acc, entry |
        class_str = entry.traces_to_current ? "current" : ""
        
        acc + content_tag(:li) do
          link_for_nav_entry(entry, class_str) +
          content_tag(:ol) do
            entry.children.inject(h '') do | acc, entry |
              acc + subnav_mobile(from_parent: entry)
            end
          end
        end
      end
      navs + content_tag(:li, (link_to '&#9776;'.html_safe, 'javascript:void(0);', onclick: "slideNavigation();"), class: 'icon')
    end
  end

  def primary_nav
    content_tag(:ol) do
      @nav.primary.inject(h '') do | acc, entry |
        class_str = entry.traces_to_current ? "current" : ""
        acc + content_tag(:li, link_for_nav_entry(entry), class: class_str)
      end
    end
  end

  def secondary_nav
    content_tag(:ol) do
      @nav.secondary.inject(h '') do | acc, entry |
        acc + subnav(from_parent: entry)
      end
    end
  end

  def subnav from_parent: nil
    # This rendering is copied from PennStation except that we are crawling
    # slightly differently structured inputs here.
    # Recursive.
    parent = from_parent
    class_str = parent.traces_to_current ? "current" : ""
    content_tag(:li, class: class_str) do
      if ! parent.children.empty?
        link_for_nav_entry(parent) +
        content_tag(:ol) do
          parent.children.
          inject(content_tag(:li,'',class: 'sub-nav-indicator')) \
          do |acc, subpage|
            if subpage.children.empty?
              (acc + content_tag(:li, link_for_nav_entry(subpage))).html_safe
            else
              (acc + subnav(from_parent: subpage)).html_safe
            end
          end # inject.
        end # ol.
      else
        link_for_nav_entry(parent)
      end # empty?
    end # li.
  end

  def subnav_mobile from_parent: nil
    # This rendering is copied from PennStation except that we are crawling
    # slightly differently structured inputs here.
    # Recursive.
    parent = from_parent
    class_str = parent.traces_to_current ? "current" : ""
    content_tag(:li) do
      if ! parent.children.empty?
        link_for_nav_entry(parent, class_str) +
        content_tag(:ol) do
          parent.children.
          inject(content_tag(:li,'', class: 'sub-nav-indicator')) \
          do |acc, subpage|
            if subpage.children.empty?
              (acc + content_tag(:li, link_for_nav_entry(subpage, class_str))).html_safe
            else
              (acc + subnav(from_parent: subpage)).html_safe
            end
          end # inject.
        end # ol.
      else
        link_for_nav_entry(parent, class_str)
      end # empty?
    end # li.
  end


  def breadcrumb_trail(page, html_options = {}, page_anchor = nil)
    content_tag(:div, build_breadcrumb_trail(page,page_anchor), html_options)
  end

  private

  def build_breadcrumb_trail(page,page_anchor)
    [
      link_to('Home','/'),
      with_breadcrumb_parent_trail(page),
      with_breadcrumb_anchor(page,page_anchor)
    ].flatten.join(' > ').html_safe
  end

  def with_breadcrumb_parent_trail(page)
    p = page
    parent_url = page.parent_url
    result = []

    while p.parent_url != '/' do
      p = PennStation::Page.where(url: p.parent_url).first
      result.unshift(link_to(p.title,p.url))
    end

    result
  end

  def with_breadcrumb_anchor(p,anchor)
    anchor.blank? ? [ p.title ] : [ link_to(p.title,p.url), anchor ]
  end
end

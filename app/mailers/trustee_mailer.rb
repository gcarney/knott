class TrusteeMailer < ActionMailer::Base
  default from: "admin@knottfoundation.org"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.trustee_mailer.password_reset.subject
  #


  def password_reset(trustee)
    @trustee = trustee
    mail :to => trustee.email, :subject => "Knott Foundation website password reset"
  end
end

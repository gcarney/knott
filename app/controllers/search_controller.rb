class SearchController < ApplicationController
  def index
    search_term = params[:search_term]
    @page = PennStation::Page.where(:url => '/search').first

    return render(:text => "Expecting a search term.", :layout => true) unless search_term.present?

    @results = Search.results(search_term, current_admin || current_trustee)
    @num_results = @results[:pages].size + @results[:blog_posts].size + @results[:grants].size
  end

end


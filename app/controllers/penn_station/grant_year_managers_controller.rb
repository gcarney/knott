module PennStation
  class GrantYearManagersController < ApplicationController
    before_action :set_grant, only: [:show, :edit, :update, :destroy]

#    track_admin_activity

    def index
      @grant_year_managers = GrantYearManager.all.order(:year)
    end

    def show
    end

    def new
      @grant_year_manager = GrantYearManager.new
    end

    def edit
    end

    def create
      @grant_year_manager = GrantYearManager.new(grant_year_manager_params)

      if @grant_year_manager.save
        redirect_to @grant_year_manager, notice: "grant year manager #{@grant_year_manager.year} was successfully created."
      else
        render action: 'new'
      end
    end

    def update
      if @grant_year_manager.update(grant_year_manager_params)
        redirect_to @grant_year_manager, notice: "Successfully updated #{@grant_year_manager.year}."
      else
        render action: 'edit'
      end
    end

    def destroy
      @grant_year_manager.destroy
      redirect_to grant_year_managers_url, notice: "grant #{@grant_year_manager.year} was successfully deleted."
    end

    private

    def set_grant
      @grant_year_manager = GrantYearManager.find(params[:id])
    end

    def grant_year_manager_params
      params.require(:grant_year_manager).permit(:year,:show_this_year)
    end
  end
end



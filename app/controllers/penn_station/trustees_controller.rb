module PennStation
  class TrusteesController < ApplicationController
    before_action :set_trustee, only: [:show, :edit, :update, :destroy]

    track_admin_activity

    def index
      @trustees = Trustee.all.order(:name)
    end

    def show
    end

    def new
      @trustee = Trustee.new
    end

    def edit
    end

    def create
      @trustee = Trustee.new(trustee_params)

      if @trustee.save
        redirect_to edit_trustee_path(@trustee), notice: "trustee #{@trustee.name} was successfully created."
      else
        render action: 'new'
      end
    end

    def update
      if @trustee.update(trustee_params)
        redirect_to edit_trustee_path(@trustee), notice: "Successfully updated trustee #{@trustee.name}."
      else
        render action: 'edit'
      end
    end

    def destroy
      @trustee.destroy
      redirect_to trustees_url, notice: "trustee #{@trustee.name} was successfully deleted."
    end

    private

    def set_trustee
      @trustee = Trustee.find(params[:id])
    end

    def trustee_params
      plist = params.require(:trustee).permit(:name, :email, :password, :password_confirmation)
    end
  end
end

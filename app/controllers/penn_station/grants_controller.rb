module PennStation
  class GrantsController < ApplicationController
    before_action :set_grant, only: [:show, :edit, :update, :destroy]

#    track_admin_activity

    def index
      @grants = Grant.all.order(:recipient)
    end

    def show
    end

    def new
      @grant = Grant.new
    end

    def edit
    end

    def create
      @grant = Grant.new(grant_params)
# g.grant_categories = GrantCategory.where(:id => [1,2,3])
      @grant.feature_on_home_page = params['grant'].has_key?('feature_on_home_page') ? true : false

      if @grant.save
        @grant.grant_categories = GrantCategory.where(:id => params['grant_categories'])
        redirect_to edit_grant_path(@grant), notice: "Grant for #{@grant.recipient} was successfully created."
      else
        render action: 'new'
      end
    end

    def update
      if @grant.update(grant_params)
        @grant.grant_categories = GrantCategory.where(:id => params['grant_categories'])
        redirect_to edit_grant_path(@grant), notice: "Successfully updated grant for #{@grant.recipient}."
      else
        render action: 'edit'
      end
    end

    def destroy
      @grant.grant_categories = []
      @grant.destroy
      redirect_to grants_url, notice: "grant #{@grant.recipient} was successfully deleted."
    end

    private

    def set_grant
      @grant = Grant.find(params[:id])
    end

    def grant_params
      params.require(:grant).permit(:recipient,:description,:value,:year,:featured_on_home_page)
    end
  end
end



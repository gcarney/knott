module PennStation
  class GrantCategoriesController < ApplicationController
    before_action :set_grant_category, only: [:show, :edit, :update, :destroy]

#    track_admin_activity

    def index
      @grant_categories = GrantCategory.all.order(:name)
    end

    def show
    end

    def new
      @grant_category = GrantCategory.new
    end

    def edit
    end

    def create
      @grant_category = GrantCategory.new(grant_category_params)

      if @grant_category.save
        redirect_to @grant_category, notice: "grant_category #{@grant_category.name} was successfully created."
      else
        render action: 'new'
      end
    end

    def update
      if @grant_category.update(grant_category_params)
        redirect_to @grant_category, notice: "Successfully updated #{@grant_category.name}."
      else
        render action: 'edit'
      end
    end

    def destroy
      @grant_category.destroy
      redirect_to grant_categories_url, notice: "grant_category #{@grant_category.name} was successfully deleted."
    end

    private

    def set_grant_category
      @grant_category = GrantCategory.find(params[:id])
    end

    def grant_category_params
      params.require(:grant_category).permit(:name)
    end
  end
end


class PagesController < ApplicationController
  before_action :load_blog_post, only: [:show]

  def show
    page = PennStation::Page.lookup(params[:url])

    if page.present?
      @page = page
      set_grant_vars if page.ptype == "PastAwards"
    else
      render :layout => 'missing_page', :template => 'shared/missing_page'
    end
  end

  private

  def load_blog_post
    if defined?(PennStationBlog) and (params[:url] == 'knott_blog')
     if params.has_key?(:post_id)
       @post = PennStationBlog::Post.find(params[:post_id])
       @posts = [ @post ]
     elsif params.has_key?(:category_id)
       @posts = PennStationBlog::Category.where(id: params[:category_id]).first.posts.where(publish: true).order('publication_date desc')
     else
       @posts = PennStationBlog::Post.where(publish: true).order('publication_date desc')
     end
    end
  end

  def set_grant_vars
    @years      = params[:grant_year_manager] || GrantYearManager.displayable_years.map{ |m| m.year.to_s }.sort
    @categories = params[:grant_category]     || GrantCategory.all.map(&:name)

    @grant_search_results = Grant.for_years(@years).with_categories(@categories)
  end
end


class TrusteesController < ApplicationController
  before_filter :authenticate_trustee
  before_filter :current_admin
  before_filter :current_trustee
  helper ::PennStation::EventsHelper

  def show
    if params[:id].nil?
      page = Trustee.page
    else
      page = PennStation::Page.where(url: "/trustees/#{params[:id]}").first
    end

    if page.present?
      @page = page
    else
      render :layout => 'missing_page', :template => 'shared/missing_page'
    end
  end

  private

  def authenticate_trustee
    unless (cookies[:auth_token] and current_trustee) or
           (session[:admin_id] and current_admin)
      redirect_to login_url
    end
  end

end

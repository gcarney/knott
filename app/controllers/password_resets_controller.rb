class PasswordResetsController < ApplicationController
  before_action :set_trustee, only: [:edit, :update]

  def new
  end

  def create
    trustee = Trustee.find_by_email(params[:email])
    trustee.send_password_reset if trustee
    redirect_to root_url, :notice => "Email sent with password reset instructions."
  end

  def edit
  end

  def update
    if @trustee.password_reset_sent_at < 2.hours.ago
      redirect_to new_password_reset_path, :alert => "Password reset has expired."
    elsif @trustee.update_attributes(trustee_params)
      params.require(:trustee).permit(:password, :password_confirmation)
      redirect_to login_path, :notice => "Password has been reset!"
    else
      render :edit
    end
  end

  private

  def set_trustee
    @trustee = Trustee.find_by_password_reset_token!(params[:id])
  end

  def trustee_params
    params.require(:trustee).permit(:password,:password_confirmation)
  end
end

class SessionsController < ApplicationController
  skip_before_filter :authenticate, :only => [:new, :create]

  def new
  end

  def create
    trustee = Trustee.where(email: params[:email]).first

    if trustee && trustee.authenticate(params[:password])
      if params[:remember_me]
        cookies.permanent[:auth_token] = trustee.auth_token
      else
        cookies[:auth_token] = trustee.auth_token
      end

      render :js => "window.location.href = '#{Trustee.url}'"
    else
      flash.now.alert = "Invalid email or password"
      render "new"
    end
  end

  def destroy
    cookies.delete(:auth_token)
    redirect_to root_url, :notice => "Logged out!"
  end
end

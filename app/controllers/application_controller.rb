require "knott_page_navigation"

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :current_admin
  before_filter :current_trustee
  helper ::PennStation::NavigationHelper
  helper ::PennStation::HeaderHelper
  helper ::PennStation::AnalyticsHelper

  private

  def current_admin
    @current_admin ||= PennStation::Admin.find(session[:admin_id]) if session[:admin_id]
  end
  helper_method :current_admin

  def current_trustee
    @current_trustee ||= Trustee.find_by_auth_token!(cookies[:auth_token]) if cookies[:auth_token]
  end
  helper_method :current_trustee
end

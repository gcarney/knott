# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140312224751) do

  create_table "grant_categories", force: true do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "grant_categorizations", force: true do |t|
    t.integer  "grant_id"
    t.integer  "grant_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "grant_year_managers", force: true do |t|
    t.integer  "year"
    t.boolean  "show_this_year"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "grants", force: true do |t|
    t.string   "recipient"
    t.text     "description"
    t.integer  "value"
    t.integer  "year"
    t.boolean  "feature_on_home_page"
    t.integer  "uploaded_image_id"
    t.string   "organization_url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "penn_station_activities", force: true do |t|
    t.string   "administrator"
    t.string   "operation"
    t.string   "info"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "penn_station_addresses", force: true do |t|
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "zipcode"
    t.text     "directions"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "penn_station_admins", force: true do |t|
    t.string   "email"
    t.string   "name"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "penn_station_analytics_settings", force: true do |t|
    t.string   "tracking_id"
    t.string   "view_id"
    t.text     "tracking_code"
    t.string   "access_token"
    t.string   "refresh_token"
    t.string   "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "penn_station_blog_categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "penn_station_blog_categories_posts", id: false, force: true do |t|
    t.integer "category_id"
    t.integer "post_id"
  end

  create_table "penn_station_blog_posts", force: true do |t|
    t.string   "title"
    t.string   "permalink_title"
    t.text     "summary"
    t.text     "body"
    t.string   "credit"
    t.boolean  "publish",          default: false
    t.date     "publication_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "penn_station_calendar_subscriptions", force: true do |t|
    t.integer  "page_id"
    t.integer  "calendar_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "penn_station_calendar_subscriptions", ["calendar_id"], name: "index_penn_station_calendar_subscriptions_on_calendar_id"
  add_index "penn_station_calendar_subscriptions", ["page_id"], name: "index_penn_station_calendar_subscriptions_on_page_id"

  create_table "penn_station_calendars", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "penn_station_calendars_scheduled_events", force: true do |t|
    t.integer  "calendar_id"
    t.integer  "scheduled_event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "penn_station_events", force: true do |t|
    t.string   "name"
    t.string   "summary"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "penn_station_folders", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "penn_station_locations", force: true do |t|
    t.string   "name"
    t.integer  "address_id"
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "penn_station_locations", ["address_id"], name: "index_penn_station_locations_on_address_id"
  add_index "penn_station_locations", ["event_id"], name: "index_penn_station_locations_on_event_id"

  create_table "penn_station_pages", force: true do |t|
    t.string   "title"
    t.string   "ptype"
    t.string   "url"
    t.integer  "position"
    t.boolean  "published"
    t.boolean  "show_in_nav"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "seo_title"
    t.string   "seo_keywords"
    t.string   "seo_description"
  end

  create_table "penn_station_roles", force: true do |t|
    t.string   "name"
    t.integer  "admin_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "penn_station_roles", ["admin_id"], name: "index_penn_station_roles_on_admin_id"

  create_table "penn_station_scheduled_events", force: true do |t|
    t.date     "starts_at"
    t.date     "ends_at"
    t.text     "times"
    t.integer  "calendar_id"
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "penn_station_scheduled_events", ["calendar_id"], name: "index_penn_station_scheduled_events_on_calendar_id"
  add_index "penn_station_scheduled_events", ["event_id"], name: "index_penn_station_scheduled_events_on_event_id"

  create_table "penn_station_sections", force: true do |t|
    t.string   "name"
    t.text     "content"
    t.integer  "page_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "moreinfo_url"
  end

  create_table "penn_station_slides", force: true do |t|
    t.string   "title"
    t.string   "caption"
    t.text     "description"
    t.string   "url"
    t.boolean  "publish"
    t.integer  "position"
    t.integer  "slideshow_id"
    t.string   "image"
    t.integer  "size"
    t.string   "content_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "penn_station_slideshows", force: true do |t|
    t.string   "name"
    t.string   "image_size_note"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "penn_station_uploaded_files", force: true do |t|
    t.string   "label"
    t.text     "caption"
    t.text     "description"
    t.integer  "folder_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file"
    t.string   "content_type"
    t.integer  "size"
  end

  create_table "trustees", force: true do |t|
    t.string   "email"
    t.string   "name"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "auth_token"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
  end

end

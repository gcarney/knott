# This migration comes from penn_station (originally 20130527191429)
class CreatePennStationRoles < ActiveRecord::Migration
  def change
    create_table :penn_station_roles do |t|
      t.string :name
      t.references :admin, index: true

      t.timestamps
    end
  end
end

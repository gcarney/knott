# This migration comes from penn_station (originally 20130422003310)
class CreatePennStationPages < ActiveRecord::Migration
  def change
    create_table :penn_station_pages do |t|
      t.string  :title,
                :ptype,
                :url

      t.integer :position

      t.boolean :published,
                :show_in_nav

      t.timestamps
    end
  end
end

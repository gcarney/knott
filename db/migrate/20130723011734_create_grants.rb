class CreateGrants < ActiveRecord::Migration
  def change
    create_table :grants do |t|
      t.string  :recipient
      t.text    :description
      t.integer :value
      t.integer :year
      t.boolean :feature_on_home_page
      t.integer :uploaded_image_id
      t.string  :organization_url

      t.timestamps
    end
  end
end

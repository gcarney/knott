# This migration comes from penn_station (originally 20131017185457)
class CreatePennStationSlideshows < ActiveRecord::Migration
  def change
    create_table :penn_station_slideshows do |t|
      t.string :name
      t.string :image_size_note

      t.timestamps
    end
  end
end

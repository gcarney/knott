class AddPasswordResetToTrustees < ActiveRecord::Migration
  def change
    add_column :trustees, :password_reset_token, :string
    add_column :trustees, :password_reset_sent_at, :datetime
  end
end

# This migration comes from penn_station (originally 20131020180114)
class CreatePennStationCalendarsScheduledEvents < ActiveRecord::Migration
  def change
    create_table :penn_station_calendars_scheduled_events do |t|
      t.references :calendar
      t.references :scheduled_event

      t.timestamps
    end
  end
end

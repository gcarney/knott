# This migration comes from penn_station (originally 20131017104417)
class AddMoreinfoUrlFieldToSections < ActiveRecord::Migration
  def change
    add_column :penn_station_sections, :moreinfo_url, :string
  end
end


class CreateTrustees < ActiveRecord::Migration
  def change
    create_table :trustees do |t|
      t.string :email
      t.string :name
      t.string :password_digest

      t.timestamps
    end
  end
end

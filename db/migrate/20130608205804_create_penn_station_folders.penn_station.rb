# This migration comes from penn_station (originally 20130608205048)
class CreatePennStationFolders < ActiveRecord::Migration
  def change
    create_table :penn_station_folders do |t|
      t.string :name

      t.timestamps
    end
  end
end

class CreateGrantCategorizations < ActiveRecord::Migration
  def self.up
    create_table :grant_categorizations do |t|
      t.integer :grant_id
      t.integer :grant_category_id

      t.timestamps
    end
  end

  def self.down
    drop_table :grant_categorizations
  end
end


class CreateGrantCategories < ActiveRecord::Migration
  def change
    create_table :grant_categories do |t|
      t.string :name
      t.string :slug

      t.timestamps
    end
  end
end

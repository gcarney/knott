class AddAuthTokenToTrustees < ActiveRecord::Migration
  def change
    add_column :trustees, :auth_token, :string
  end
end

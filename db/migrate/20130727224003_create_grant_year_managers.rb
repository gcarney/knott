class CreateGrantYearManagers < ActiveRecord::Migration
  def change
    create_table :grant_year_managers do |t|
      t.integer :year
      t.boolean :show_this_year

      t.timestamps
    end
  end
end

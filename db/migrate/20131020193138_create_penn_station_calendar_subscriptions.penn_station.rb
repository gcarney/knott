# This migration comes from penn_station (originally 20131019170151)
class CreatePennStationCalendarSubscriptions < ActiveRecord::Migration
  def change
    create_table :penn_station_calendars do |t|
      t.string :name

      t.timestamps
    end

    create_table :penn_station_calendar_subscriptions do |t|
      t.references :page, index: true
      t.references :calendar, index: true

      t.timestamps
    end
  end
end

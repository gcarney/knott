# This migration comes from penn_station (originally 20130608221742)
class CreatePennStationUploadedFiles < ActiveRecord::Migration
  def change
    create_table :penn_station_uploaded_files do |t|
      t.string :label
      t.text :caption
      t.text :description

      t.references :folder

      t.timestamps
    end
  end
end

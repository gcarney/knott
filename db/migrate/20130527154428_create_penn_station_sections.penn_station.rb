# This migration comes from penn_station (originally 20130429155541)
class CreatePennStationSections < ActiveRecord::Migration
  def change
    create_table :penn_station_sections do |t|
      t.string :name
      t.text :content

      t.references :page

      t.timestamps
    end
  end
end

# This migration comes from penn_station (originally 20131019173126)
class CreatePennStationScheduledEvents < ActiveRecord::Migration
  def change
    create_table :penn_station_scheduled_events do |t|
      t.date   :starts_at
      t.date   :ends_at
      t.text   :times

      t.references :calendar, index: true
      t.references :event, index: true

      t.timestamps
    end

    create_table :penn_station_events do |t|
      t.string :name
      t.string :summary
      t.text   :description

      t.timestamps
    end

    create_table :penn_station_addresses do |t|
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zipcode
      t.text   :directions

      t.timestamps
    end

    create_table :penn_station_locations do |t|
      t.string :name

      t.references :address, index: true
      t.references :event, index: true

      t.timestamps
    end
  end
end

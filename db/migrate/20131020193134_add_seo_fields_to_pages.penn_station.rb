# This migration comes from penn_station (originally 20131016104417)
class AddSeoFieldsToPages < ActiveRecord::Migration
  def change
    add_column :penn_station_pages, :seo_title,       :string
    add_column :penn_station_pages, :seo_keywords,    :string
    add_column :penn_station_pages, :seo_description, :string
  end
end


# This migration comes from penn_station (originally 20130422003200)
class CreatePennStationActivities < ActiveRecord::Migration
  def change
    create_table :penn_station_activities do |t|
      t.string :administrator
      t.string :operation
      t.string :info

      t.timestamps
    end
  end
end

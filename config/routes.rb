Knott::Application.routes.draw do
  get "password_resets/new"
  root :to => 'pages#show', :url => '/'

  mount PennStation::Engine, :at => "admin"
  mount PennStationBlog::Engine, :at => "admin/blog"

  get "logout" => "sessions#destroy", :as => "logout"
  get "login"  => "sessions#new",     :as => "login"

  get "password_resets/new"
  resources :password_resets

  resources :sessions
  resources :pages

  PennStation::Engine.routes.draw do
    resources :grants
    resources :grant_categories
    resources :grant_year_managers
    resources :trustees
  end

  post "search" => "search#index"

  match '/trustees' => 'trustees#show', :via => :get
  match '/trustees/:id' => 'trustees#show', :via => :get
  match '*url' => 'pages#show', :via => :get

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end

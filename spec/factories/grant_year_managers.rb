# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :grant_year_manager do
    year 1
    show_this_year false
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :grant_category, :class => 'GrantCategory' do
    name "MyString"
    slug "MyString"
  end
end


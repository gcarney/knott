# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :grant, :class => 'Grant' do
    recipient "MyString"
    description "MyString"
    value 1000
    year 2000
    feature_on_home_page "false"
    organization_url "MyString"
  end
end


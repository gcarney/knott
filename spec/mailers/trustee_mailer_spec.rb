require "spec_helper"

describe TrusteeMailer do
  describe "password_reset" do
    let(:mail) { TrusteeMailer.password_reset(FactoryGirl.create(:trustee)) }

    it "renders the headers" do
      mail.subject.should eq("Password reset")
      mail.to.should eq(["to@example.org"])
      mail.from.should eq(["from@example.com"])
    end

    it "renders the body" do
      mail.body.encoded.should match("Hi")
    end
  end

end


require 'spec_helper'

describe GrantCategory do
  let(:grant_category) { FactoryGirl.build(:grant_category) }

  context "when a grant_category is valid" do
    it "should have a name" do
      grant_category.should respond_to(:name)
    end

    it "should have a slug" do
      grant_category.should respond_to(:slug)
    end

    it "should be valid" do
      grant_category.should be_valid
    end
  end

  context "when grant_categories are not valid" do
    it "should fail when the name is missing" do
      grant_category = FactoryGirl.build(:grant_category, name: nil)
      grant_category.should_not be_valid
    end
  end
end

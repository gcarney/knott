require 'spec_helper'

describe Grant do
  let(:grant) { FactoryGirl.build(:grant) }

  context "when a grant is valid" do
    it "should have a recipient" do
      grant.should respond_to(:recipient)
    end

    it "should have a year" do
      grant.should respond_to(:year)
    end

    it "should have a description" do
      grant.should respond_to(:description)
    end

    it "should have a value" do
      grant.should respond_to(:value)
    end

    it "should have a feature_on_home_page" do
      grant.should respond_to(:feature_on_home_page)
    end

    it "should have a organization_url" do
      grant.should respond_to(:feature_on_home_page)
    end

    it "should have an uploaded_image_id" do
      grant.should respond_to(:uploaded_image_id)
    end

    it "should be valid" do
      grant.should be_valid
    end
  end

  context "when grants are not valid" do
    it "should fail when the recipient is missing" do
      grant = FactoryGirl.build(:grant, recipient: nil)
      grant.should_not be_valid
    end

    it "should fail when the year is missing" do
      grant = FactoryGirl.build(:grant, year: nil)
      grant.should_not be_valid
    end
  end
end

require 'spec_helper'

describe GrantYearManager do
  let(:grant_year_manager) { FactoryGirl.build(:grant_year_manager) }

  context "when a grant_year_manager is valid" do
    it "should have a year" do
      grant_year_manager.should respond_to(:year)
    end

    it "should have a show_this_year" do
      grant_year_manager.should respond_to(:show_this_year)
    end

    it "should be valid" do
      grant_year_manager.should be_valid
    end
  end

  context "when grant_year_managers are not valid" do
    it "should fail when the year is missing" do
      grant = FactoryGirl.build(:grant_year_manager, year: nil)
      grant.should_not be_valid
    end

    it "should fail when the show_this_year is missing" do
      grant = FactoryGirl.build(:grant_year_manager, show_this_year: nil)
      grant.should_not be_valid
    end
  end
end
